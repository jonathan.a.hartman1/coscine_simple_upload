/**************************************************************************************************
 * Event Listeners
 *************************************************************************************************/
document.querySelector("#submit-token-form").addEventListener("submit", validateToken);
document.querySelector("#project-select").addEventListener("change", handleProjectSelect);
document.querySelector("#resource-select").addEventListener("change", handleResourceSelect);

/**************************************************************************************************
 * Global Variables
 *************************************************************************************************/
let droppedFiles = [];

/**************************************************************************************************
 * API POST Requests
 *************************************************************************************************/

/**
 * Asynchronously sends a POST request to the specified URL with the specified data.
 *
 * @param {string} url - The URL to send the POST request to.
 * @param {Object} data - The data to send in the POST request.
 * @param {string} method - The HTTP method to use.
 * @returns {Promise<Object>} The response data as a JavaScript object.
 */
async function fetchData(url = '', data = {}, method = "GET") {
    // Send a request to the specified URL with the specified data
    const response = await fetch(url, {
        method: method,
        headers: {
            "Content-Type": "application/json"
        },
        body: method === "POST" ? JSON.stringify(data) : null
    });

    // If the response was not ok, throw an error
    if (!response.ok) {
        throw new Error(`HTTP error ${response.status}`);
    }

    // Parse the response data as JSON
    return response.json();
}

/**
 * Asynchronously validates a token by sending a POST request.
 *
 * @param {Event} event - The submit event from the form.
 */
async function validateToken(event) {
    // Prevent the form from submitting normally
    event.preventDefault();

    // Get the value of the token input field
    const token = document.querySelector("#token").value;

    try {
        // Send a POST request to validate the token
        const data = await fetchData("/validate_token", { token: token }, method = "POST");

        console.log('Response received:', data);

        if (data.success) {
            handleValidToken();
        } else {
            alert(`Token is invalid: ${data.error}`);
        }
    } catch (error) {
        console.error('An error occurred:', error);
        alert(`An error occurred while validating the token: ${error.message}`);
    }
}

/**
 * Handles the case where the token is valid.
 * Disables the token input field and Validate Token button, and switches to the next tab.
 * Also gets the project data from the /project_list endpoint and updates the project-select
 * input field.
 */
function handleValidToken() {
    // Disable the token input field and Validate Token button
    document.querySelector("#token").disabled = true;

    console.log(document.querySelector("#submit-token"));

    document.querySelector("#submit-token").disabled = true;

    // Automatically switch to the next tab
    var nextTabPane = $("#project-tab");
    nextTabPane.tab('show');
    nextTabPane.removeClass("disabled");

    // Disable the token-tab
    var tokenTabPane = $("#token-tab");
    tokenTabPane.addClass("disabled");

    // Get data from the /project_list endpoint
    fetchData("/project_list")
        .then(data => {
            console.log('Response received:', data);
            // Update the project-select input field with the project data
            updateProjectSelect(data);
        })
        .catch(error => {
            console.error('An error occurred:', error);
            alert(`An error occurred while getting the project data: ${error.message}`);
        });
}

/**
 * Handles the user selecting an item from the "project-select" select input.
 * This function is called whenever the user changes the selected option in the "project-select" input field.
 */
function handleProjectSelect() {
    // Get the selected option
    const selectedOption = document.querySelector("#project-select").value;

    // Automatically switch to the next tab
    var nextTabPane = $("#resource-tab");
    nextTabPane.tab('show');
    nextTabPane.removeClass("disabled");

    // Call the /resource_list endpoint with the selected option as the project_id
    fetchData(`/resource_list?project_name=${selectedOption}`)
        .then(data => {
            console.log('Response received:', data);
            // Update the resource-select input field with the resource data
            updateResourceSelect(data);
        })
        .catch(error => {
            console.error('An error occurred:', error);
            alert(`An error occurred while getting the resource data: ${error.message}`);
        });
}

/**
 * Handles the user selecting an item from the "resource-select" select input.
 * This function is called whenever the user changes the selected option in the "resource-select" input field.
 */
function handleResourceSelect() {
    // Get the selected option
    const selectedProject = document.querySelector("#project-select").value;
    const selectedResource = document.querySelector("#resource-select").value;

    // Automatically switch to the next tab
    var nextTabPane = $("#metadata-tab");
    nextTabPane.tab('show');
    nextTabPane.removeClass("disabled");

    // Call the /resource_list endpoint with the selected option as the project_id
    fetchData(`/application_profile?project_name=${selectedProject}&resource_name=${selectedResource}`)
        .then(data => {
            console.log('Response received:', data);
            // Update the metadata form with the received data
            updateMetadataForm(data);
        })
        .catch(error => {
            console.error('An error occurred:', error);
            alert(`An error occurred while getting the resource data: ${error.message}`);
        });
}

/**
 * Handles the user submitting the metadata form.
 *
 * @param {Event} event - The submit event from the form.
 */
function handleMetadataSubmit(event) {
    // Prevent the form from submitting normally
    event.preventDefault();

    // Get the form from the event target
    const metadataForm = event.target;

    // Get the values of the input fields
    const formData = new FormData(metadataForm);
    for (const [key, value] of formData.entries()) {
        formData[key] = value;
    }

    // Add the dropped files to the form data
    for (let i = 0; i < droppedFiles.length; i++) {
        formData.append("files[]", droppedFiles[i]);
    }

    // Get the Project and Resource names from the select inputs
    const projectName = document.querySelector("#project-select").value;
    const resourceName = document.querySelector("#resource-select").value;

    // Add the Project and Resource names to the form data
    formData.append("project_name", projectName);
    formData.append("resource_name", resourceName);

    // Send a POST request to the /submit endpoint with the form data
    fetch("/upload", {
        method: "POST",
        body: formData
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            if (data.success) {
                alert("Submission successful!");
            } else {
                alert(`Submission failed: ${data.error}`);
            }
        })
        .catch(error => {
            console.error('An error occurred:', error);
            alert(`An error occurred while submitting the data: ${error.message}`);
        });
}

/**************************************************************************************************
 * DOM Manipulation
 *************************************************************************************************/

/**
 * Updates the project-select input field with the given project data.
 *
 * @param {Array} projects - The project data.
 */
function updateProjectSelect(projects) {
    const projectSelect = document.querySelector("#project-select");
    projectSelect.innerHTML = "";

    // Add a placeholder option as the first option
    const placeholderOption = document.createElement("option");
    placeholderOption.value = "";
    placeholderOption.text = "-- Select a project --";
    placeholderOption.style.color = "grey";
    placeholderOption.disabled = true;
    placeholderOption.selected = true;
    projectSelect.appendChild(placeholderOption);

    projects.forEach(project => {
        const option = document.createElement("option");
        option.value = project.name;
        option.text = project.name;
        projectSelect.appendChild(option);
    });

    // Add an event listener to the change event to remove the placeholder option when the user
    // selects a project
    projectSelect.addEventListener("change", function () {
        // If the placeholder option still exists, remove it
        if (placeholderOption.parentNode) {
            placeholderOption.parentNode.removeChild(placeholderOption);
        }
    });
}

/**
 * Updates the resource-select input field with the given resource data.
 *
 * @param {Array} resources - The resource data.
 */
function updateResourceSelect(resources) {
    const resourceSelect = document.querySelector("#resource-select");
    resourceSelect.innerHTML = "";

    // Add a placeholder option as the first option
    const placeholderOption = document.createElement("option");
    placeholderOption.value = "";
    placeholderOption.text = "-- Select a resource --";
    placeholderOption.style.color = "grey";
    placeholderOption.disabled = true;
    placeholderOption.selected = true;
    resourceSelect.appendChild(placeholderOption);

    resources.forEach(resource => {
        const option = document.createElement("option");
        option.value = resource.name;
        option.text = resource.name;
        resourceSelect.appendChild(option);
    });

    // Add an event listener to the change event to remove the placeholder option when the user
    // selects a resource
    resourceSelect.addEventListener("change", function () {
        // If the placeholder option still exists, remove it
        if (placeholderOption.parentNode) {
            placeholderOption.parentNode.removeChild(placeholderOption);
        }
    });
}

/**
* Updates the metadata form with the given field data.
*
* @param {Array} fields - The field data as JSON, retrieved from the /application_profile endpoint.
*/
function updateMetadataForm(fields) {
    // Iterate over the fields. For each field, create a new input element and add it to the form.
    // The type of input element depends on the field type
    const metadataForm = document.querySelector("#metadata-form");
    metadataForm.innerHTML = "";

    // Add the drag-and-drop area to the form
    const dropArea = createDropArea();
    metadataForm.appendChild(dropArea);

    fields.forEach(field => {
        // Create a label for the input
        const label = document.createElement("label");
        label.for = field.name;
        label.textContent = field.name;
        metadataForm.appendChild(label);

        let input;
        if (field.vocabulary) {
            // Create a select input if the field contains the key "vocabulary"
            input = document.createElement("select");
            field.vocabulary.forEach(optionValue => {
                const option = document.createElement("option");
                option.value = optionValue;
                option.text = optionValue;
                input.appendChild(option);
            });
        } else if (field.datatype === 'date') {
            // Create a date input if the field type is 'date'
            input = document.createElement("input");
            input.type = 'date';

            // TEMP MAYBE
            let today = new Date();
            let day = String(today.getDate()).padStart(2, '0');
            let month = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            let year = today.getFullYear();
            today = year + '-' + month + '-' + day;
            input.value = today;

        } else if (field.datatype === 'int') {
            // Create a number input if the field type is 'int'
            input = document.createElement("input");
            input.type = 'number';
            input.value = 0;
        } else {
            // Create a regular input otherwise
            input = document.createElement("input");
            input.type = "text";
            input.placeholder = field.name;
        }
        input.id = field.name;
        input.name = field.name;
        input.required = field.required;
        input.classList.add("form-control");
        metadataForm.appendChild(input);
    });

    // Add a submit button to the form
    const submitButton = document.createElement("button");
    submitButton.type = "submit";
    submitButton.textContent = "Submit";
    submitButton.classList.add("btn");
    submitButton.classList.add("btn-primary");
    metadataForm.appendChild(submitButton);

    metadataForm.addEventListener("submit", handleMetadataSubmit);
}

/**
 * Creates a drag-and-drop area for the user to drop files into.
 * Will work with both files and folders.
 *
 * @returns {HTMLElement} The drag-and-drop area as an HTML element.
 */
function createDropArea() {
    // Create a drag-and-drop area
    const dropArea = document.createElement("div");
    dropArea.textContent = "Drag and drop file(s) or a folder here";
    dropArea.style.border = "2px dashed #aaa";
    dropArea.style.padding = "20px";
    dropArea.style.marginBottom = "20px";
    dropArea.style.textAlign = "center";
    dropArea.style.color = "#aaa";
    dropArea.style.cursor = "pointer";

    // Create a hidden file input element with the webkitdirectory attribute
    const fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.webkitdirectory = true;
    fileInput.style.display = "none";
    dropArea.appendChild(fileInput);

    // Create a new div for displaying the filenames
    const fileDisplayArea = document.createElement("div");
    fileDisplayArea.id = "file-display-area";
    fileDisplayArea.style.marginTop = "20px";

    // Append fileDisplayArea to the body or any other parent element
    dropArea.appendChild(fileDisplayArea);

    // When the drop area is clicked, trigger the file input click event
    dropArea.addEventListener("click", function () {
        fileInput.click();
    });

    // When files are selected, handle the files
    fileInput.addEventListener("change", function () {
        handleFiles(this.files);
    });

    // Handle the dropped files
    async function handleFiles(items) {
        const fileDisplayArea = document.getElementById('file-display-area');

        for (let i = 0; i < items.length; i++) {
            const item = items[i].webkitGetAsEntry();
            if (item) {
                if (item.isFile) {
                    item.file(file => {
                        console.log(file);
                        displayFileName(file);
                        droppedFiles.push(file);
                    });
                } else if (item.isDirectory) {
                    const directoryReader = item.createReader();
                    directoryReader.readEntries(async entries => {
                        for (let entry of entries) {
                            if (entry.isFile) {
                                entry.file(file => {
                                    console.log(file);
                                    displayFileName(file);
                                    droppedFiles.push(file);
                                });
                            } else if (entry.isDirectory) {
                                // Recursively call handleFiles if the entry is a subdirectory
                                await handleFiles([entry]);
                            }
                        }
                    });
                }
            }
        }

        function displayFileName(file) {
            const filenameDiv = document.createElement('div');
            filenameDiv.textContent = file.name;
            fileDisplayArea.appendChild(filenameDiv);
        }
    }

    // Add event listeners to handle the drag-and-drop functionality
    dropArea.addEventListener("dragover", function (event) {
        event.preventDefault();
        dropArea.style.backgroundColor = "#eee";
    });
    dropArea.addEventListener("dragleave", function (event) {
        dropArea.style.backgroundColor = "transparent";
    });
    dropArea.addEventListener("drop", function (event) {
        event.preventDefault();
        dropArea.style.backgroundColor = "transparent";
        handleFiles(event.dataTransfer.items);
    });

    return dropArea;
}