from datetime import date, datetime
import logging
import os

import coscine
from coscine.exceptions import NotFoundError
from flask import Flask, render_template, request, jsonify
from werkzeug.utils import secure_filename

try:
    from dotenv import load_dotenv

    load_dotenv()
except ModuleNotFoundError:
    pass


app = Flask(__name__)
logger = logging.getLogger(__name__)

CLIENT = None


@app.route("/")
def index():
    return render_template("index.html.j2")


@app.route("/validate_token", methods=["POST"])
def validate_token() -> dict:
    global CLIENT

    token = request.json.get("token") if request.json else None
    logger.info(f"Received token: {token is not None}")

    # If the user doesn't provide a token and we have a special test token in the environment,
    # use that.
    if not token:
        token = os.getenv("COSCINE_TEST_TOKEN")

    # If there's still no token, return an error
    if not token:
        logger.error("No token provided")
        return jsonify({"success": False, "error": "No token provided"}), 400

    try:
        # Try to create a client with the token and access a property to validate the token
        CLIENT = coscine.ApiClient(token)
        CLIENT.self().display_name
        logger.info("Token is valid")
        return jsonify({"success": True}), 200
    except PermissionError:
        # If a PermissionError is raised, the token is invalid
        logger.error("Invalid token")
        return jsonify({"success": False, "error": "Invalid token"}), 401
    except Exception as e:
        # If any other exception is raised, something else went wrong
        logger.error(f"An error occurred: {str(e)}")
        return jsonify({"success": False, "error": "An error occurred"}), 500


@app.route("/project_list", methods=["GET"])
def project_list() -> list[dict]:
    global CLIENT
    try:
        project_list = CLIENT.projects(toplevel=False)
    except NotFoundError:
        return jsonify([]), 404

    projects = [
        {"name": project.display_name, "id": project.id} for project in project_list
    ]
    return jsonify(projects)


@app.route("/resource_list", methods=["GET"])
def resource_list() -> list[dict]:
    global CLIENT
    project_name = request.args.get("project_name", default=None, type=str)

    try:
        resource_list = CLIENT.project(project_name).resources()
    except NotFoundError:
        return jsonify([]), 404

    resources = [
        {"name": resource.name, "id": resource.id} for resource in resource_list
    ]
    return jsonify(resources)


@app.route("/application_profile", methods=["GET"])
def application_profile() -> list[dict]:
    global CLIENT
    project_name = request.args.get("project_name", default=None, type=str)
    resource_name = request.args.get("resource_name", default=None, type=str)

    application_profile = (
        CLIENT.project(project_name).resource(resource_name).application_profile
    )

    field_list = convert_application_profile_to_json(application_profile)

    return jsonify(field_list)


@app.route("/upload", methods=["POST"])
def upload() -> dict:
    global CLIENT
    project_name = request.form.get("project_name")
    project = CLIENT.project(project_name)

    resource_name = request.form.get("resource_name")
    resource = project.resource(resource_name)

    metadata_form = convert_form_to_metadata(request.form, resource)

    if "files[]" not in request.files:
        return "No file part", 400
    files = request.files.getlist("files[]")
    for file in files:
        if file.filename == "":
            return "No selected file", 400
        if file:
            filename = secure_filename(file.filename)
            resource.upload(filename, file.stream.read(), metadata_form)
    return {"success": True}


def convert_form_to_metadata(form_data, resource) -> coscine.MetadataForm:
    form = resource.metadata_form()
    for key, value in form_data.items():
        if key not in form:
            continue

        field = form.field(key)
        if field.datatype == date:
            value = datetime.strptime(value, r"%Y-%m-%d").date()
        elif field.datatype == int:
            value = int(value)

        form[key] = value

    return form


def convert_application_profile_to_json(application_profile) -> list[dict]:
    fields = []
    for field in application_profile.fields():
        field_dict = {}
        field_dict["name"] = field.name
        field_dict["datatype"] = field.datatype.__name__
        field_dict["is_required"] = field.is_required

        if field.has_vocabulary:
            field_dict["vocabulary"] = field.vocabulary.keys()

        fields.append(field_dict)

    return fields
